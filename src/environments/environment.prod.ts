import { domain, clientId, audience, serverUrl } from '../../auth_config.json';

export const environment = {
    production: true,
    auth: {
      domain,
      clientId,
      redirectUri: window.location.origin + '/profile',
      audience
    },
    gatewayUrl: "https://quackergatewayservice.westeurope.cloudapp.azure.com"
  };