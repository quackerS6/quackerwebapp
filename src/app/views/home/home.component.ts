import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '@auth0/auth0-angular';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  // Todo: Is this how the redirect should work?
  constructor(private auth: AuthService, private router: Router) {
    const observer = {
      next: (isLoggedIn: boolean) => {
        if(isLoggedIn)
          this.router.navigate(['/profile']);
      }
    };

    this.auth.isAuthenticated$.subscribe(observer);

   }

  ngOnInit(): void {

  }

}
