import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '@auth0/auth0-angular';
import { IFollowStatePageViewModel } from '../viewmodels/IFollowStatePageViewModel';
import { environment as env } from 'src/environments/environment';

@Component({
  selector: 'app-follows',
  templateUrl: './follows.component.html',
  styleUrls: ['./follows.component.scss']
})
export class FollowsComponent implements OnInit {

  hasProfileLoaded: boolean = false;
  userProfile?: any;
  ProfilePromise?: Promise<any>;
  followTabIndex: number = 0;
  pageTitle: string[] = ['Is followed by', 'Is following'];
  tag?: string; 
  Model: IFollowStatePageViewModel;

  constructor(public auth: AuthService, private rest: HttpClient, private route: ActivatedRoute) {
    this.Model = {tag: '', imageUrl: '', name:'', following: [], followers: [], followerCount: 0, followingCount: 0}
  }

  ngOnInit(): void {

    const routeParams = this.route.snapshot.paramMap;
    this.tag = routeParams.get('tag') as string;

    this.ProfilePromise = new Promise<any>((resolve, _) => {
      this.auth.user$.subscribe(profile => {
        resolve(profile);
      });
    }).then(() => {
        this.rest.get(`${env.gatewayUrl}/quackapi/users/${this.tag}?include=following&include=followers&isTag=true`)
          .subscribe((result) => {
            this.Model = result as IFollowStatePageViewModel;
            console.log(this.Model);
          });
          setTimeout(() => this.hasProfileLoaded = true, 20);
    });

  }

}
