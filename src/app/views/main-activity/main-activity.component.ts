import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '@auth0/auth0-angular';
import TimeAgo from 'javascript-time-ago';
import en from 'javascript-time-ago/locale/en';
import { IMainActivityPageViewModel } from '../viewmodels/IMainActivityPageViewModel';
import { MainActivityPageViewModel } from '../viewmodels/MainActivityPageViewModel';
import { environment as env } from 'src/environments/environment';

@Component({
  selector: 'app-main-activity',
  templateUrl: './main-activity.component.html',
  styleUrls: ['./main-activity.component.scss']
})
export class MainActivityComponent implements OnInit {

  hasProfileLoaded: boolean = false;
  userProfile?: any;
  userId?: string;
  ProfilePromise?: Promise<any>;
  Model: IMainActivityPageViewModel;
  timeCalculator: TimeAgo;
  quackText: string = '';


  constructor(public auth: AuthService, private rest: HttpClient) {
    // Todo: make the time ago a service.
    this.Model = new MainActivityPageViewModel();
    TimeAgo.addDefaultLocale(en);
    this.timeCalculator = new TimeAgo('en-US');
  }

  ngOnInit(): void {

    this.ProfilePromise = new Promise<any>((resolve, _) => {
      this.auth.user$.subscribe(profile => {
        resolve(profile);
      });
    }).then(receivedProfile => {
      this.userId = receivedProfile['https://quacker.live/quackerid'];
        this.rest.get(`${env.gatewayUrl}/home/${this.userId}?include=mentions&include=timeline&isTag=false`)
          .subscribe((result) => {
            this.Model = result as IMainActivityPageViewModel;
            console.log(this.Model);
          });
          setTimeout(() => this.hasProfileLoaded = true, 20);
    });

  }

  timeAgo(date: string): string{
    return this.timeCalculator.format(Date.parse(date), 'twitter-now'); 
  }

  toggleFollowUser(e: Event) {
    const userToFollowId = (e.currentTarget as HTMLDivElement).getAttribute('data-userid');
      this.rest.post(`${env.gatewayUrl}/quackapi/followers/${userToFollowId}`, {}).toPromise().then(() => {
        this.Model.followRecommendations = this.Model.followRecommendations.filter(x => x.id != userToFollowId);
      });
  }

  postQuack(){
    this.rest.post(`${env.gatewayUrl}/quackapi/quacks`, {text: this.quackText, userId: this.userId, hashTags: this.getHashtagsFromTweet(this.quackText), mentions: this.getMentionsFromTweet(this.quackText) }).toPromise().then((data: any) => {
      const quack = {id: data.id, text: data.text, postDate: new Date().toLocaleString('en-US'), userInfo: {tag: this.Model.tweetProfile.tag, name: this.Model.tweetProfile.name, imageUrl: this.Model.tweetProfile.imageUrl}};
      this.Model.tweetProfile.quacksOnTimeline?.unshift(quack);
    });
    this.quackText = ''
  }

  //Todo: this should be in the quack token component so it can be reused
  private getHashtagsFromTweet(tweetText: string): string[] {
    const hashtagRegex = /(:?\s|^)#[a-zA-Z0-9]+/g;
    const hashTags: RegExpMatchArray = tweetText.match(hashtagRegex) as RegExpMatchArray;
    if(hashTags)
      return hashTags.map(hashTag => hashTag.split('#')[1]?.trim());
    else
      return [];
  }

  private getMentionsFromTweet(tweetText: string): string[] {
    const mentionRegex = /(:?\s|^)@[a-zA-Z0-9]+/g;
    const mentions: RegExpMatchArray = tweetText.match(mentionRegex) as RegExpMatchArray;
    if(mentions)
      return mentions.map(hashTag => hashTag.split('@')[1].trim());
    else
      return [];
  }


}

