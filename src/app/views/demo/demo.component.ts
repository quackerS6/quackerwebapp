import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import {SafeResourceUrl} from '@angular/platform-browser';

@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.scss']
})
export class DemoComponent implements OnInit {
  @ViewChild('videoFrame') videoFrame?: ElementRef; 
  videos: DemoVideo[] = [
    new DemoVideo('sign up', 'https://drive.google.com/file/d/1AvBBXm7I84XebNQ8KeGghA89cNmPzUHm/preview'),
    new DemoVideo('sending quacks', 'https://drive.google.com/file/d/10qXrM2pRz-PW-LQJqEoV8EuA7w7qfsYL/preview'),
    new DemoVideo('hashtags and mentions', 'https://drive.google.com/file/d/11QmCAYJvCOzrn4idwZmv9XAxsOr19ETL/preview'),
    new DemoVideo('main activity page', 'https://drive.google.com/file/d/1ZKEOyJdCYIkiphJKAM5PniNK78gIyPRS/preview'),
    new DemoVideo('following others', 'https://drive.google.com/file/d/1v0WuM2yw4OHg4DZkZcA69_z-8xg8urDq/preview')
  ];
  videoIndex: number = 0;
  activeSrc: SafeResourceUrl = '';

  constructor() {
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void{
    this.setCurrentSrc();
  }

  setVideoIndex(value: number) {
      value = Math.max(0, value);
      this.videoIndex = Math.min(this.videos.length - 1, value);
      this.setCurrentSrc()
  }

  setCurrentSrc(){
      // This is ok, because the url's are all hardcoded and not derived from user input
      this.videoFrame!.nativeElement.src = this.videos[this.videoIndex].url;
  }

}


class DemoVideo {
  public static globalIndex: number = 0;
  public index: number;
  constructor(public title:string, public url: string){
    this.index = DemoVideo.globalIndex;
    DemoVideo.globalIndex ++;
  }
}
