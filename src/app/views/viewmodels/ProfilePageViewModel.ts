import { IProfilePageViewModel } from "./IProfilePageViewModel";

export class ProfilePageViewModel implements IProfilePageViewModel {
    public tweetProfile: any;
    public userProfile: any;

    constructor(){
        this.tweetProfile = {
            quacks: [],
            tag: 'PlaceholderTag',
            isFollowing: false
        }

        this.userProfile = {
            firstname: 'PlaceholderName',
            isAskedForOptional: true
        }
    }
}