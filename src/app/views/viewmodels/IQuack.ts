import { IUserInfoInQuack } from "./IUserInfoInQuack";

export interface IQuack {
    id: string,
    text: string,
    postDate: string,
    userInfo?: IUserInfoInQuack
}