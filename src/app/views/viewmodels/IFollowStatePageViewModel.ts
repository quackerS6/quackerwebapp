import { IFollowRecommendation } from "./IFollowRecommendation";

export interface IFollowStatePageViewModel {
    tag: string,
    imageUrl: string,
    name: string,
    followers: IFollowRecommendation[],
    followerCount: number;
    following: IFollowRecommendation[]
    followingCount: number;
}