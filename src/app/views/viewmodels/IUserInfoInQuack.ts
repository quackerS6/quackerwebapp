export interface IUserInfoInQuack {
    tag: string,
    name: string,
    imageUrl: string
}