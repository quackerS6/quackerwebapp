export interface IHashtag {
    hashtag: string,
    timesQuacked: number
}