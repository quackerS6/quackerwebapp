import { IFollowRecommendation } from "./IFollowRecommendation";
import { IHashtag } from "./IHashtag";
import { IQuack } from "./IQuack";

export interface IMainActivityPageViewModel {
    hashtagTrends: IHashtag[],
    followRecommendations: IFollowRecommendation[],
    tweetProfile: {
        tag: string,
        name: string,
        imageUrl: string,
        quacksOnTimeline: IQuack[],
        mentions: IQuack[]
    }
}