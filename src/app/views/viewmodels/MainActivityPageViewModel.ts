import { IFollowRecommendation } from "./IFollowRecommendation";
import { IHashtag } from "./IHashtag";
import { IMainActivityPageViewModel } from "./IMainActivityPageViewModel";
import { IQuack } from "./IQuack";

export class MainActivityPageViewModel implements IMainActivityPageViewModel {
    
    public hashtagTrends: IHashtag[];
    public followRecommendations: IFollowRecommendation[];
    public tweetProfile: any; 

    constructor() {
        this.hashtagTrends = [];
        this.followRecommendations = [];
        this.tweetProfile = {
            tag: '',
            name: '',
            imageUrl: '',
            quacksOnTimeline: [],
            mentions: []
        }
    }
}