import { IQuack } from "./IQuack";

export interface IProfilePageViewModel {
    userProfile: {
        id: string,
        firstname: string,
        lastname: string,
        bio: string,
        isAskedForOptionalInfo: boolean,
        city: string,
        country: string
    },

    tweetProfile: {
        tag: string,
        name: string,
        imageUrl: string
        quacks: IQuack[],
        quacksOnTimeline: IQuack[],
        isFollowing: boolean,
        quackCount: number,
        followerCount: number,
        followingCount: number
    }
}
