export interface IFollowRecommendation {
    id: string,
    tag: string,
    name: string,
    imageUrl: string
}