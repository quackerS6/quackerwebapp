import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '@auth0/auth0-angular';
import { IProfilePageViewModel } from '../viewmodels/IProfilePageViewModel';
import { ProfilePageViewModel } from '../viewmodels/ProfilePageViewModel';
import TimeAgo from 'javascript-time-ago';
import en from 'javascript-time-ago/locale/en'
import { ActivatedRoute } from '@angular/router';
import { environment as env } from 'src/environments/environment';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.scss']
})
export class ProfilePageComponent implements OnInit {

  isOwnerOfProfile: boolean = false;
  isModalActive: boolean = false;
  userProfile?: any;
  userId?: string;
  ProfilePromise?: Promise<any>;
  Model: IProfilePageViewModel;
  defaultText = '';
  quackText = this.defaultText;
  timeCalculator: TimeAgo;
  hasProfileLoaded = false;
  quackTabIndex = 0;

  toggleModal(){
    this.isModalActive =  !this.isModalActive;
  }

  constructor(public auth: AuthService, private rest: HttpClient, private route: ActivatedRoute) {
    this.Model = new ProfilePageViewModel();
    // Todo: make the time ago a service.
    TimeAgo.addDefaultLocale(en);
    this.timeCalculator = new TimeAgo('en-US');
  }

  async ngOnInit(): Promise<void> {
    const routeParams = this.route.snapshot.paramMap;
    let userTag = routeParams.get('tag') as string;
    this.ProfilePromise = new Promise<any>((resolve, _) => {
      this.auth.user$.subscribe(profile => {
        resolve(profile);
      });
    }).then(receivedProfile => {
      this.userId = receivedProfile['https://quacker.live/quackerid'];
      let getProfileUrl = `${env.gatewayUrl}/profile/`;
      this.isOwnerOfProfile = userTag === null;
      if(! this.isOwnerOfProfile){
        getProfileUrl += `${userTag}?isTag=true`;
      }
      else{
        getProfileUrl += `${this.userId}?isTag=false`;
      }
      getProfileUrl += `&include=timeline&include=quacks&userId=${this.userId}`;
      this.rest.get(getProfileUrl)
      .subscribe((result) => {
        this.Model = result as IProfilePageViewModel;
        console.log(this.Model);
      });
      setTimeout(() => this.hasProfileLoaded = true, 20);
    });

  }
 
  postQuack(){
    this.rest.post(`${env.gatewayUrl}/quackapi/quacks`, {text: this.quackText, userId: this.userId, hashTags: this.getHashtagsFromTweet(this.quackText), mentions: this.getMentionsFromTweet(this.quackText) }).toPromise().then((data: any) => {
      const tweet = {id: data.id, text: data.text, postDate: new Date().toLocaleString('en-US'), userInfo: {tag: this.Model.tweetProfile.tag, name: this.Model.tweetProfile.name, imageUrl: this.Model.tweetProfile.imageUrl}};
      this.Model.tweetProfile.quacksOnTimeline?.unshift(tweet);
      this.Model.tweetProfile.quacks?.unshift(tweet);
      this.Model.tweetProfile.quackCount ++;
    });
    this.isModalActive = false;
    this.quackText = this.defaultText;
  }

  toggleFollowUser(e: Event) {
    const userToFollowId = (e.currentTarget as HTMLDivElement).getAttribute('data-userid');
    if(this.Model.tweetProfile.isFollowing) {
      this.rest.delete(`${env.gatewayUrl}/quackapi/followers/${userToFollowId}`, {}).toPromise().then(() => {
        this.Model.tweetProfile.isFollowing = false;
        this.Model.tweetProfile.followerCount --;
      });

    }
    else{
      this.rest.post(`${env.gatewayUrl}/quackapi/followers/${userToFollowId}`, {}).toPromise().then(() => {
        this.Model.tweetProfile.isFollowing = true;
        this.Model.tweetProfile.followerCount ++;
      });
    }

  }

  charactersLeft(): number {
    return 144 - this.quackText.length;
    //return Math.max(0, 144 - this.quackText.length);
  }


  timeAgo(date: string): string{
    return this.timeCalculator.format(Date.parse(date), 'twitter-now'); 
  }

  private getHashtagsFromTweet(tweetText: string): string[] {
    const hashtagRegex = /(:?\s|^)#[a-zA-Z0-9]+/g;
    const hashTags: RegExpMatchArray = tweetText.match(hashtagRegex) as RegExpMatchArray;
    if(hashTags)
      return hashTags.map(hashTag => hashTag.split('#')[1]?.trim());
    else
      return [];
  }

  private getMentionsFromTweet(tweetText: string): string[] {
    const mentionRegex = /(:?\s|^)@[a-zA-Z0-9]+/g;
    const mentions: RegExpMatchArray = tweetText.match(mentionRegex) as RegExpMatchArray;
    if(mentions)
      return mentions.map(hashTag => hashTag.split('@')[1].trim());
    else
      return [];
  }

  // TODO: Fix performance as angular has no computed properties (probably just make a component that does this)
  tweetToRenderArray(tweetText: string): {isMatch: boolean, text: string}[] {
    let regex = /(?:\s|^)(?:#|@)[a-zA-Z0-9]+/g;
    let toRender = [];
    let result = tweetText.split(regex);
    let matches = tweetText.match(regex) || [];
    
    for(let i=0; i < result.length; i++) {
        toRender.push({isMatch: false, text: result[i]});
        if(matches.length >= i) {
          toRender.push({isMatch: true, text: matches[i]});
        }
    }
    return toRender;
  }
  
}
