import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormHelperAsync } from '../../../classes/validation/FormHelperAsync';
import { FormFieldAsync } from '../../../classes/validation/FormFieldAsync';
import { FormField } from '../../../classes/validation/FormField';
import { isFilledIn, maxLength, wrapperAsync } from '../../../classes/validation/FormValidation';
import _ from 'lodash';
import { IProfilePageViewModel } from '../../viewmodels/IProfilePageViewModel';
import { ProfilePageViewModel } from '../../viewmodels/ProfilePageViewModel';
import { FormHelper } from 'src/app/classes/validation/FormHelper';
import { environment as env } from 'src/environments/environment';

@Component({
  selector: 'app-sign-up-form',
  templateUrl: './sign-up-form.component.html',
  styleUrls: ['./sign-up-form.component.scss']
})
export class SignUpFormComponent implements OnInit {
  // @Input() tag: string = '';
  // @Input() firstName: string = '';
  @Input() profile: IProfilePageViewModel = new ProfilePageViewModel();
  //@Output() profileChange = new EventEmitter<string>();
  //@Input() isAskedForOptional: boolean = false;

  lastName?: string;
  country?: string;
  city?: string;
  isOptionalModalActive: boolean = true;
  isTagModalActive: boolean = true;

  tagFormHelper?: FormHelperAsync;
  tagForm: any;
  optionalForm: any;

  tagError = '';
  firstNameError = '';

  constructor(private rest: HttpClient) {
    this.debouncedValidateTag = _.debounce(this.debouncedValidateTag, 1000);
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(){
    this.tagForm = {
      id: "tagForm",
      firstname: new FormField(isFilledIn(), maxLength(50, 'this field cannot be longer than 50 characters')),
      tag: new FormFieldAsync(isFilledIn(), maxLength(20, 'this field cannot be longer than 20 characters'), wrapperAsync(this.isTagAvailable.bind(this), 'this tag is not available'))
    };
    this.optionalForm = {
      id: "optionalForm",
      lastname: new FormField(),
      country: new FormField(),
      city: new FormField()
    }
    this.tagFormHelper = new FormHelperAsync(this.tagForm,{
        validClassName: "is-success",
        invalidClassName: "is-danger",
        parentValidClassName: "is-success",
        parentInvalidClassName: "is-danger",
        debug: true,
        submitIfValid: false,
        parentbeforeClassName: "is-loading"
    });

   new FormHelper(this.optionalForm); // Bind the fields

    this.tagFormHelper.onValid = () => {
      this.tagError = '';
      this.firstNameError = '';
    }
    this.tagFormHelper.onInvalid = () => {
      this.tagError = this.tagForm.tag.errors[0] || '';
      this.firstNameError = this.tagForm.firstname.errors[0] || '';
    }
  }

  areAllEmpty(...str: string[]): boolean {
    return str.every(x => this.isEmpty(x));
  }

  isEmpty(str: string): boolean {
    return (!str || str.length === 0 );
  }

  isTagAvailable(input: string): Promise<boolean>{
    // Todo: don't run check too often
    return this.rest.get(`${env.gatewayUrl}/quackapi/users/tag/${input}`)
      .toPromise()
      .then(result => {
        console.log(result);
        return result === null; // No content response means available
      }).catch(_ => {
        return false;
      });
  }

  setTagModalState(){
    this.isTagModalActive = this.isEmpty(this.profile.userProfile.firstname) || this.isEmpty(this.profile.tweetProfile.tag);
    //this.isTagModalActive = true;
  }

  setOptionalModalState(){
    this.isOptionalModalActive = (! this.isTagModalActive) && (! this.profile.userProfile.isAskedForOptionalInfo); 
  }

  TagModalState(): boolean{
    return (this.isEmpty(this.profile.userProfile.firstname) || this.isEmpty(this.profile.userProfile.firstname)) && this.isTagModalActive;
    //return this.isTagModalActive;
  }

  OptionalModalState(): boolean{
    return ((! this.TagModalState()) && (! this.profile.userProfile.isAskedForOptionalInfo)) && this.isOptionalModalActive; 
  }

  finishTagModal(){
    //this.isTagModalActive = false;
    // this.setOptionalModalState();
    this.tagFormHelper?.validateFormOnClickAsync().then(isValid => {
      if(isValid){
        this.isTagModalActive = false;
        this.setOptionalModalState();

        this.rest.patch(`${env.gatewayUrl}/userapi/users/${this.profile.userProfile.id}`, {firstName: this.tagForm.firstname.value, tag: this.tagForm.tag.value }).toPromise().then(_ => {
          this.profile.userProfile.firstname = this.tagForm.firstname.value;
          this.profile.tweetProfile.tag = this.tagForm.tag.value;
        });

      }
    });
  }

  closeOptionalModal(isSkipped = true){
    let patchData: Object;
    if(isSkipped){
      patchData = {
        isAskedForOptionalInfo: true
      }
    }
    else {
      patchData = {
        lastName: this.optionalForm.lastname.value,
        country: this.optionalForm.country.value,
        city: this.optionalForm.city.value,
        isAskedForOptionalInfo: true 
      }
    }
      this.rest.patch(`${env.gatewayUrl}/userapi/users/${this.profile.userProfile.id}`, patchData).toPromise()
      .then(_ => {
        if(! isSkipped){
          this.profile.userProfile.lastname = this.optionalForm.lastname.value;
          this.profile.userProfile.country = this.optionalForm.country.value;
          this.profile.userProfile.city = this.optionalForm.city.value;
        }
        this.isOptionalModalActive = false;
      });
    }

  validateTag(){
    this.tagFormHelper?.beforeFieldValidation(this.tagForm.tag);
    this.debouncedValidateTag();
  }

  errorText(isTag = true){
    if(isTag){
      return this.tagForm.tag.errors[0] || '';
    }
    else{
      return this.tagForm.firstname.errors[0] || '';
    }
  }

  debouncedValidateTag(){
    const result: Promise<any> = this.tagForm.tag.validate();
    // Todo: make this easier to access
    result.then(_ => {
      this.tagFormHelper?.afterFieldValidation(this.tagForm.tag);
      if(this.tagForm.tag.isValid){
        this.tagError = '';
      }
      else{
        this.tagError = this.tagForm.tag.errors[0];
      }
    });
  }

}
