import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Quacker';
  
  constructor(@Inject(DOCUMENT) private document: Document){

  }

  ngOnInit(): void {
     const theme = localStorage.getItem('theme') as string;
     this.document.body.setAttribute('data-theme', theme);
  }
}
