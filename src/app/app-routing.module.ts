import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@auth0/auth0-angular';
import { DemoComponent } from './views/demo/demo.component';
import { FollowsComponent } from './views/follows/follows.component';
import { HomeComponent } from './views/home/home.component';
import { MainActivityComponent } from './views/main-activity/main-activity.component';
import { ProfilePageComponent } from './views/profile-page/profile-page.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'profile', component: ProfilePageComponent, canActivate: [AuthGuard]},
  {path: 'profile/:tag', component: ProfilePageComponent},
  {path: 'home', component: MainActivityComponent, canActivate: [AuthGuard]},
  {path: 'follows/:tag', component: FollowsComponent},
  {path: 'demo', component: DemoComponent}
  // {path: 'housekeeping', component: AdminDashboardComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
