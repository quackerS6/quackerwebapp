import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './views/home/home.component';
import { NavbarComponent } from './navbar/navbar.component';

// Auth0, imports the module from the SDK
import { AuthModule } from '@auth0/auth0-angular';
import { AuthButtonComponent } from './components/auth-button/auth-button.component';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { ProfilePageComponent } from './views/profile-page/profile-page.component';
import { PercentCircleComponent } from './components/percent-circle/percent-circle.component';
import { QuackAreaComponent } from './components/quack-area/quack-area.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

// Http stuff
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthHttpInterceptor } from '@auth0/auth0-angular';
import { environment as env } from 'src/environments/environment';
import { SignUpFormComponent } from './views/profile-page/sign-up-form/sign-up-form.component';
import { LoaderComponent } from './components/loader/loader.component';
import { OptionMenuComponent } from './components/option-menu/option-menu.component';
import { ThemeOptionsComponent } from './components/option-menu/theme-options/theme-options.component';
import { TokenTextComponent } from './components/token-text/token-text.component';
import { MainActivityComponent } from './views/main-activity/main-activity.component';
import { FollowsComponent } from './views/follows/follows.component';
import { DemoComponent } from './views/demo/demo.component';

// Environment

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    AuthButtonComponent,
    UserProfileComponent,
    ProfilePageComponent,
    PercentCircleComponent,
    QuackAreaComponent,
    SignUpFormComponent,
    LoaderComponent,
    OptionMenuComponent,
    ThemeOptionsComponent,
    TokenTextComponent,
    MainActivityComponent,
    FollowsComponent,
    DemoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    // Auth0, imports the module into the application, with configuration
    AuthModule.forRoot({
      ...env.auth,
      httpInterceptor: {
        allowedList: [`${env.gatewayUrl}/*`]
      }
    })
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthHttpInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
