import { IFormLogger } from './IFormLogger'

export class NullableLogger implements IFormLogger {
    debugLog(data: any) {}
    debugTable(data: any) {}
}