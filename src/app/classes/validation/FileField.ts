
import { FormElement } from "./FormElement";
import { fieldValidatorAll } from "./FormValidation";

export class FileField extends FormElement<FileList> {
    
    constructor(...args: fieldValidatorAll[]){
        super(...args); // For readability
    }
    
    /**
     * @override from FormElement
     */ 
    get value(): FileList {
        return this.bindedElement.files as FileList;
    }

    /**
     * @override from FormElement
     */
    set value(val: FileList){
        this.bindedElement.files = val;
    }

    /**
     * @override from FormElement
     * cannot clear file field
     */
    clear(){

    }
}