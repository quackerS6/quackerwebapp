import { FormHelper } from './FormHelper'

export class FormHelperAsync extends FormHelper {
    
    /**
     * The async variant of the method in the base class
     * Waits async for the form validation to be finished
     */
    setupFormSubmit(){
        if(! this._formElement){
            throw "Form element cannot be undefined";
        }
        this._formElement.onsubmit = event => {
            this.onSubmit();
            if(this._preventSubmit){
                alert("I GET HERE!");
                event.preventDefault();
            }
            this.validateFormAsync().then(isValid => {
                if(isValid){
                    this.onValid();
                    if(this._options?.submitIfValid){
                        this.submitForm();
                    }
                }
                else{
                    this.onInvalid();
                }
            });
        }
    }
    
    
    /**
     * The async variant of the method in the base class
     * @returns {Promise} promise with the result of the form validation
     */
    private validateFormAsync(): Promise<boolean>{
        const values: any = {};
        const promises: Promise<void>[] = [];

        for(const prop in this._form){
            const formField = this._form[prop];
            this.beforeFieldValidation(formField);
            formField.otherValues = values;
            values[prop] = formField.value; 
            promises.push(Promise.resolve(formField.validate()));
        }

        return Promise.all(promises).then(results => {
           this._formFields.forEach(formField => {
               this.afterFieldValidation(formField);
           });
           return results.every(r => r);
        });
    }

    /**
     * Allows binding to an on click for submitting the form
     * @remark allows the clicked button to be outside the form for submitting
     */
         public validateFormOnClickAsync(): Promise<boolean> {
            return this.validateFormAsync().then(isValid => {
                if(isValid){
                    this.onValid();
                }
                else{
                    this.onInvalid();
                }
                return isValid;
            });

        }


}