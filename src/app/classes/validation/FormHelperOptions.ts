/**
 * An option object that can be passed in a formfield helper to customize the settings
 */
export class FormHelperOptions {

    public validClassName?: string;
    public invalidClassName?: string;
    public beforeClassName?: string;

    public parentValidClassName?: string;
    public parentInvalidClassName?: string;
    public parentbeforeClassName?: string;

    public debug?: boolean;
    public submitIfValid?: boolean;

    constructor(){
        this.setDefaults();
    }

    private setDefaults(): void {
        this.validClassName = "valid",
        this.invalidClassName = "invalid",
        this.beforeClassName = "loading",
        this.parentValidClassName = "valid"
        this.parentInvalidClassName = "invalid"
        this.parentbeforeClassName = "loading"
        this.debug = false,
        this.submitIfValid =  false
    }
}