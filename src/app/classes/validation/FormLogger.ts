import { FormElement } from "./FormElement";
import { IFormLogger } from "./IFormLogger";
import { FormHelperOptions } from "./FormHelperOptions";
import { NullableLogger } from "./NullableLogger";

export class FormLogger implements IFormLogger {

    private static _instance?: IFormLogger;

    private constructor(){
        // Singleton    
    }

    public static get instance(): IFormLogger {
        return this._instance as IFormLogger;
    }

    public static initInstance(isLoggingEnabled: boolean): IFormLogger {
        if(! this._instance){
            if(isLoggingEnabled){
                this._instance = new FormLogger();
            }
            else{
                this._instance = new NullableLogger();
            }
        }
        return this._instance as IFormLogger;
    }

    debugLog (message: any){
        console.log(`%cDebug: ${message} `, 'background-color: #333; color: #efefef; padding: 2px;');
    }
    debugTable (message: any[]){
        console.table(message);
    }



}