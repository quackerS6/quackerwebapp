export interface IFormLogger {
    debugLog: (data: any) => void;
    debugTable: (data: any[]) => void;
}