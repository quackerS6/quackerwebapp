import { HttpClient } from '@angular/common/http';
import { Component, Inject, Input, OnInit } from '@angular/core';
import { AuthService } from '@auth0/auth0-angular';
import { NavProfile } from './navbar.types';
import { environment as env } from 'src/environments/environment';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  profile: NavProfile = {imageUrl: '', name: '', userId: ''};
  userId?: string;
  isMenuActive = false;
  storageItemName = 'nav-profile';

  constructor(private rest: HttpClient, public auth: AuthService) {

  }

  async ngOnInit(): Promise<void> {
    new Promise<any>((resolve, _) => {
      this.auth.user$.subscribe(profile => {
        resolve(profile);
      });
    }).then(async receivedProfile => {
      this.userId = receivedProfile['https://quacker.live/quackerid'];
      this.profile = await this.getProfile();
    });

  }

  toggleMenu() {
    this.isMenuActive = !this.isMenuActive;
  }

  // Loading profile
  async getProfile(): Promise<NavProfile> {
    let loadedProfile = this.getProfileFromLocalStorage();
    if(! loadedProfile || loadedProfile.userId != this.userId) {
      loadedProfile = await this.getProfileFromServer();
      localStorage.setItem(this.storageItemName, JSON.stringify(loadedProfile));
    }
    return loadedProfile;
  }

  private getProfileFromServer(): Promise<NavProfile> {
    // Todo: make backend so that i don't need to include quacks etc. 
    return this.rest.get(`${env.gatewayUrl}/quackapi/users/${this.userId}`).toPromise().then(profile => profile as NavProfile);
  }

  private getProfileFromLocalStorage(): NavProfile{
    return JSON.parse(localStorage.getItem(this.storageItemName) as string);
  }

  logout(){
    this.auth.logout({returnTo: document.location.origin});
  }



}
