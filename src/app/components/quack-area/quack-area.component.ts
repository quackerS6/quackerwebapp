import { ThisReceiver } from '@angular/compiler';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-quack-area',
  templateUrl: './quack-area.component.html',
  styleUrls: ['./quack-area.component.scss']
})
export class QuackAreaComponent {

  private _quackText?: string;

  @Input() set quackText(value: string) {
    this._quackText = value;
    this.hiddenTextAllowed = this.quackText.substring(0, this.allowedCharacters);
    this.hiddenTextNotAllowed = this.quackText.substring(this.allowedCharacters);
  }

  get quackText(): string {
    return this._quackText as string;
  }

  @Output() quackTextChange = new EventEmitter<string>();

  hiddenTextAllowed: string = '';
  hiddenTextNotAllowed: string = '';
  readonly allowedCharacters: number;

  constructor() {
    this.allowedCharacters = 144;  
  }

  updateText(text: string) {
    this.quackText = text;
    this.quackTextChange.emit(this.quackText);
  }


}
