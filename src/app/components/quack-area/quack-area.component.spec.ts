import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuackAreaComponent } from './quack-area.component';

describe('QuackAreaComponent', () => {
  let component: QuackAreaComponent;
  let fixture: ComponentFixture<QuackAreaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuackAreaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuackAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
