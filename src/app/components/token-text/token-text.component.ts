import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-token-text',
  templateUrl: './token-text.component.html',
  styleUrls: ['./token-text.component.scss']
})
export class TokenTextComponent implements OnInit {

  private readonly regex: RegExp = /(?:\s|^)(?:#|@)[a-zA-Z0-9]+/g;
  toRender: TextPart[] = [];

  @Input() set text(value: string) {
    this.toRender = [];
    const result = value.split(this.regex);
    const matches = value.match(this.regex) || [];

    for(let i=0; i < result.length; i++) {
        this.toRender.push(new TextPart(false, result[i]));
        if(matches.length > 0 && matches.length >= i) {
          this.toRender.push(new TextPart(true, matches[i]));
        }
    }
  }

  constructor() {}

  ngOnInit(): void {}


}

class TextPart {
  constructor(public isMatch: boolean, public text: string) {}
}