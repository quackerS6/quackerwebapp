import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TokenTextComponent } from './token-text.component';

describe('TokenTextComponent', () => {
  let component: TokenTextComponent;
  let fixture: ComponentFixture<TokenTextComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TokenTextComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TokenTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
