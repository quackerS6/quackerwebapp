import { DOCUMENT } from '@angular/common';
import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-theme-options',
  templateUrl: './theme-options.component.html',
  styleUrls: ['../option-menu.component.scss']
})
export class ThemeOptionsComponent implements OnInit {

  @Output()
  closeMenu: EventEmitter<boolean> = new EventEmitter<boolean>();
  theme: string = 'dark'

  constructor(@Inject(DOCUMENT) private document: Document) { }

  ngOnInit(): void {
    this.theme = localStorage.getItem('theme') as string;
  }

  setTheme(isDark: boolean) {
    const value = isDark ? 'dark' : 'light';
    this.document.body.setAttribute('data-theme', value);   
    localStorage.setItem('theme', value);
    this.closeMenu.emit(true);
  }

}
