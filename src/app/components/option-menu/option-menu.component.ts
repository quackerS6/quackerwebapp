import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AuthService } from '@auth0/auth0-angular';

@Component({
  selector: 'app-option-menu',
  templateUrl: './option-menu.component.html',
  styleUrls: ['./option-menu.component.scss']
})
export class OptionMenuComponent implements OnInit {

  @Output()
  closeMenu: EventEmitter<void> = new EventEmitter();

  isThemeOptionsActive = false;

  constructor(private auth: AuthService) { }

  ngOnInit(): void {
  }

  toggleThemeOptions() {
    this.isThemeOptionsActive = !this.isThemeOptionsActive;
  }

  closeThemeOptions(closeOptionMenu: boolean){
    this.isThemeOptionsActive = false;
    if(closeOptionMenu){
      this.closeMenu.emit();
    }
  }

  logout(){
    this.auth.logout({returnTo: document.location.origin});
  }

}
