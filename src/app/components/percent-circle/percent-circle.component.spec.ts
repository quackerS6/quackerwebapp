import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PercentCircleComponent } from './percent-circle.component';

describe('PercentCircleComponent', () => {
  let component: PercentCircleComponent;
  let fixture: ComponentFixture<PercentCircleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PercentCircleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PercentCircleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
