import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-percent-circle',
  templateUrl: './percent-circle.component.html',
  styleUrls: ['./percent-circle.component.scss']
})
export class PercentCircleComponent{

  @Input() total: number = 100;
  @Input() current: number = 0;

  constructor() { }

  percentage() : number{
    return Math.min(100, Math.floor(this.current / this.total * 100));
  }

}
