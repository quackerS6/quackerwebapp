import { Component, ElementRef, Inject, OnInit, Renderer2, ViewChild } from '@angular/core';
import { AuthService } from '@auth0/auth0-angular';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-auth-button',
  templateUrl: './auth-button.component.html',
  styles: ['./auth-button.component.scss'],
})
export class AuthButtonComponent {
  constructor(@Inject(DOCUMENT) public document: Document, public auth: AuthService) {
  }

  ngAfterViewInit(){
    this.document.querySelector('#login')?.addEventListener('click', () => {
      this.auth.loginWithRedirect({
        appState: {target: '/profile'},
        screen_hint: 'login'
      });
    });



    // For the signup button
    this.document.querySelector('#signup')?.addEventListener('click', () => {
      this.auth.loginWithRedirect({
        appState: {target: '/profile'},
        screen_hint: 'signup'
      });
    });
  }

}